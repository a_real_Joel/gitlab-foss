import $ from 'jquery';

// change to onecommons ?
export default function initLogoAnimation() {
  window.addEventListener('beforeunload', () => {
    $('.tanuki-logo').addClass('animate');
  });
}
